import { render, screen } from '@testing-library/react';
import Header from './header'
import userEvent from '@testing-library/user-event'

const CALL_API_MOCK = jest.fn();


describe("example for  unit test", () => {
    beforeEach(() => {
        render(<Header callAPI={CALL_API_MOCK}/>);
    })
    it ("Should be render text header", () => {
        
        const text = screen.getByText(/halo/i);
        expect(text). toBeInTheDocument();
    });

    test("btn should be functional when you clicked", () => {
        
        const btnTest = screen.getByTestId("btn-test");
        userEvent.click(btnTest);
        expect(CALL_API_MOCK).toHaveBeenCalled()
    });

});